#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <unistd.h>
#include <iostream>
#include <fcntl.h>
#include <sys/mman.h>
  
    
int main(int argc, char *argv[])
{
	if(argc!=3) {
		std::cout << "usage:" << std::endl;
		std::cout << "sudo ./kr enable delay" << std::endl;
		std::cout << "sudo ./kr 1 1000000" << std::endl;
		return 0;
	}
   uint32_t enable = (uint32_t)strtol(argv[1], NULL, 10);
   uint32_t speed = (uint32_t)strtol(argv[2], NULL, 10);

   unsigned int mem_size = 0x100;
   off_t pbase = 0x41600000; // physical base address
   uint32_t *base_addr;
   int fd;
  
   if ((fd = open("/dev/mem", O_RDWR | O_SYNC)) != -1) {
		base_addr = (uint32_t *)mmap(NULL, mem_size, PROT_READ|PROT_WRITE, MAP_SHARED, fd, pbase);

		base_addr[0] = enable;
		base_addr[1] = speed;

		std::cout << "base_addr[0]: " << base_addr[0] << std::endl;
		std::cout << "base_addr[1]: " << base_addr[1] << std::endl;
		std::cout << "base_addr[2]: " << std::hex << base_addr[2] << std::endl;
		std::cout << "base_addr[3]: " << std::hex << base_addr[3] << std::endl;
		std::cout << "base_addr[4]: " << std::hex << base_addr[4] << std::endl;

      close(fd);
   } else {
   	std::cout << "failed to open /dev/mem" << std::endl;
   }
 }