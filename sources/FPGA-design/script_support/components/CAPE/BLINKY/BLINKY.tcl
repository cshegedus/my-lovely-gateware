puts "BLINKY.tcl start"

import_files -hdl_source {script_support/components/CAPE/BLINKY/HDL/blinky.v}

build_design_hierarchy

# Exporting Create HDL core command for module blinky
set my_hdl_file ${project_dir}/hdl/blinky.v
puts $my_hdl_file
create_hdl_core -file "$my_hdl_file" -module {blinky}


# Exporting BIF information of  HDL core command for module blinky
hdl_core_add_bif -hdl_core_name {blinky} -bif_definition {APB:AMBA:AMBA2:slave} -bif_name {BIF_1} -signal_map {\
"PADDR:PADDR" \
"PENABLE:PENABLE" \
"PWRITE:PWRITE" \
"PRDATA:PRDATA" \
"PWDATA:PWDATA" \
"PREADY:PREADY" \
"PSLVERR:PSLVERR" \
"PSELx:PSELx" }

# Creating SmartDesign "CAPE_BLINKY"
set sd_name {CAPE_BLINKY}
create_smartdesign -sd_name ${sd_name}

# Disable auto promotion of pins of type 'pad'
auto_promote_pad_pins -promote_all 0

# Create top level Scalar Ports
sd_create_scalar_port -sd_name ${sd_name} -port_name {APBslave_PENABLE} -port_direction {IN}
sd_create_scalar_port -sd_name ${sd_name} -port_name {APBslave_PSELx} -port_direction {IN}
sd_create_scalar_port -sd_name ${sd_name} -port_name {APBslave_PWRITE} -port_direction {IN}
sd_create_scalar_port -sd_name ${sd_name} -port_name {PCLK} -port_direction {IN}
sd_create_scalar_port -sd_name ${sd_name} -port_name {PRESETN} -port_direction {IN}

sd_create_scalar_port -sd_name ${sd_name} -port_name {APBslave_PREADY} -port_direction {OUT}
sd_create_scalar_port -sd_name ${sd_name} -port_name {APBslave_PSLVERR} -port_direction {OUT}


# Create top level Bus Ports
sd_create_bus_port -sd_name ${sd_name} -port_name {APBslave_PADDR} -port_direction {IN} -port_range {[7:0]}
sd_create_bus_port -sd_name ${sd_name} -port_name {APBslave_PWDATA} -port_direction {IN} -port_range {[31:0]}

sd_create_bus_port -sd_name ${sd_name} -port_name {APBslave_PRDATA} -port_direction {OUT} -port_range {[31:0]}
sd_create_bus_port -sd_name ${sd_name} -port_name {kr_blink} -port_direction {OUT} -port_range {[11:0]}


# Create top level Bus interface Ports
sd_create_bif_port -sd_name ${sd_name} -port_name {APBslave} -port_bif_vlnv {AMBA:AMBA2:APB:r0p0} -port_bif_role {slave} -port_bif_mapping {\
"PADDR:APBslave_PADDR" \
"PSELx:APBslave_PSELx" \
"PENABLE:APBslave_PENABLE" \
"PWRITE:APBslave_PWRITE" \
"PRDATA:APBslave_PRDATA" \
"PWDATA:APBslave_PWDATA" \
"PREADY:APBslave_PREADY" \
"PSLVERR:APBslave_PSLVERR" } 

# Add blinky_0 instance
sd_instantiate_hdl_core -sd_name ${sd_name} -hdl_core_name {blinky} -instance_name {blinky_0}



# Add scalar net connections
sd_connect_pins -sd_name ${sd_name} -pin_names {"PCLK" "blinky_0:PCLK" }
sd_connect_pins -sd_name ${sd_name} -pin_names {"PRESETN" "blinky_0:PRESETN" }

# Add bus net connections
sd_connect_pins -sd_name ${sd_name} -pin_names {"kr_blink" "blinky_0:kr_blink" }

# Add bus interface net connections
sd_connect_pins -sd_name ${sd_name} -pin_names {"APBslave" "blinky_0:BIF_1" }

# Re-enable auto promotion of pins of type 'pad'
auto_promote_pad_pins -promote_all 1
# Save the SmartDesign 
save_smartdesign -sd_name ${sd_name}
# Generate SmartDesign "CAPE_BLINKY"
generate_component -component_name ${sd_name}

puts "BLINKY.tcl end"
