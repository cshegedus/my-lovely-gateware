module blinky(
    input    PCLK,
    input    PRESETN,
    input  [31:0] PADDR,
    input         PENABLE,
    output [31:0] PRDATA,
    output        PREADY,
    input         PSELx,
    output        PSLVERR,
    input  [31:0] PWDATA,
    input         PWRITE,
    output [11:0]  kr_blink
);



    reg [31:0] control;
    reg [31:0] countLimit;
    reg [31:0] status;



    reg [31:0] counter;
    reg shift;
    always@(posedge PCLK or negedge PRESETN) begin
        if(~PRESETN) begin
            counter <= 32'h0;
            shift <= 1'b0;
        end else begin
            if((counter==countLimit) || resetCounter) begin
                counter <= 32'h0;
                shift <= 1'b1;
            end else begin
                counter <= counter + 1;
                shift <= 1'b0;
            end
        end
    end

    wire shift_sw;
    assign shift_sw = shift && control[0];
    //assign shift_sw = shift;
    reg shift_sw_r;

    always@(posedge PCLK or negedge PRESETN) begin
        if(~PRESETN) begin
            shift_sw_r <= 1'b0;
        end else begin
            shift_sw_r <= shift_sw;
        end
    end

    reg dir;
    reg [11:0] kr_blink_r;
    
    always@(posedge PCLK or negedge PRESETN) begin
        if(~PRESETN) begin
            dir <= 1'b0;
        end else begin
            if(shift_sw && (kr_blink_r[0] || kr_blink_r[11])) begin
                dir <= !dir;
            end
        end
    end
    
    always@(posedge PCLK or negedge PRESETN) begin
        if(~PRESETN) begin
            kr_blink_r <= 12'h20;
        end else begin
            if(shift_sw_r) begin
                if(dir) begin
                    kr_blink_r <= {kr_blink_r[10:0], 1'b0};
                end else begin
                    kr_blink_r <= {1'b0,kr_blink_r[11:1]};
                end
            end
        end
    end
    
    assign kr_blink = kr_blink_r;







    reg PREADY_r;
    always @(posedge PCLK) begin
        if (PRESETN == 0) begin
            PREADY_r <= 1'b1;
        end
    end
    assign PREADY = PREADY_r;

    reg PSLVERR_r;
    always @(posedge PCLK) begin
        if (PRESETN == 0) begin
            PSLVERR_r <= 1'b0;
        end
    end
    assign PSLVERR = PSLVERR_r;

    always @(posedge PCLK) begin
        if (PRESETN == 0) begin
            control   <= 32'h0;
            status   <= 32'h0;
            countLimit <= 32'h00100000;
        end else begin
            if (PSELx && PWRITE && PENABLE) begin
                case (PADDR[7:0])
                    8'h00 : control <= PWDATA;
                    8'h04 : countLimit <= PWDATA;
                    default : ;
                endcase
            end
        end
    end
    
    wire resetCounter;
    assign resetCounter = PSELx && PWRITE && PENABLE;

    reg [31:0] PRDATA_r;
    always @(posedge PCLK) begin
        if (PRESETN == 0) begin
            PRDATA_r   <= 32'h0;
        end else begin
            if (PSELx) begin
                case (PADDR[7:0])
                    8'h00   : PRDATA_r <= control;
                    8'h04   : PRDATA_r <= countLimit;
                    8'h08   : PRDATA_r <= status;
                    8'h0c   : PRDATA_r <= 32'hABBAEDDA;
                    8'h10   : PRDATA_r <= 32'hDEADFACE;
                    default : PRDATA_r <= 32'h0;
                endcase
            end else begin
                PRDATA_r <= 32'h0;
            end
        end
    end

    
    assign PRDATA = PRDATA_r;


endmodule

